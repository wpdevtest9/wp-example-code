<?php

class Kindful_Login_Admin {

	const MENU_SLUG = 'kindful-login';

	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kindful-login-admin.css', array(), $this->version, 'all' );

	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kindful-login-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function admin_menu() {

		add_menu_page(
			__( 'Kindful Login', 'kindful-login' ),
			__( 'Kindful Login', 'kindful-login' ),
			'manage_options',
			self::MENU_SLUG,
			array( $this, 'display_settings_page' ),
			'dashicons-universal-access-alt'
		);
	}

	public function display_settings_page() {

		require plugin_dir_path( __FILE__ ) . 'partials/kindful-login-admin-display.php';
	}

	public function admin_init() {

		$section_id = Kindful_Login::PREFIX . '_general_setting_section';

		add_settings_section(
			$section_id,
			'',
			'',
			self::MENU_SLUG
		);

		// organization subdomain
		add_settings_field(
			Kindful_Login::PREFIX . Kindful_Login::ORGANIZATION_SUBDOMAIN_FIELD_NAME,
			__( 'Organization subdomain', 'kindful-login' ),
			array( $this, 'organization_subdomain_field_callback' ),
			self::MENU_SLUG,
			$section_id
		);

		// application Id
		add_settings_field(
			Kindful_Login::PREFIX . Kindful_Login::APPLICATION_ID_FIELD_NAME,
			__( 'Application Id', 'kindful-login' ),
			array( $this, 'application_id_field_callback' ),
			self::MENU_SLUG,
			$section_id
		);

		// secret
		add_settings_field(
			Kindful_Login::PREFIX . Kindful_Login::SECRET_FIELD_NAME,
			__( 'Secret', 'kindful-login' ),
			array( $this, 'secret_field_callback' ),
			self::MENU_SLUG,
			$section_id
		);

		// Callback URL
		add_settings_field(
			Kindful_Login::PREFIX . Kindful_Login::CALLBACK_URL_FIELD_NAME,
			__( 'Callback URL', 'kindful-login' ),
			array( $this, 'callback_url_field_callback' ),
			self::MENU_SLUG,
			$section_id
		);


		register_setting( self::MENU_SLUG, Kindful_Login::PREFIX . Kindful_Login::PLAYGROUND_FIELD_NAME );

		register_setting( self::MENU_SLUG, Kindful_Login::PREFIX . Kindful_Login::ORGANIZATION_SUBDOMAIN_FIELD_NAME );

		register_setting( self::MENU_SLUG, Kindful_Login::PREFIX . Kindful_Login::APPLICATION_ID_FIELD_NAME );

		register_setting( self::MENU_SLUG, Kindful_Login::PREFIX . Kindful_Login::SECRET_FIELD_NAME );

		register_setting( self::MENU_SLUG, Kindful_Login::PREFIX . Kindful_Login::CALLBACK_URL_FIELD_NAME, array(
			'sanitize_callback' => array( $this, 'sanitize_url_field' )
		) );
	}

	public function playground_field_callback() {

		?>
		<input type="checkbox"
		       class="regular-text"
		       name="<?php echo esc_attr( Kindful_Login::PREFIX . Kindful_Login::PLAYGROUND_FIELD_NAME ); ?>"
		       <?php checked( true, Kindful_Login::is_playground() ); ?>>
		<?php
	}

	public function organization_subdomain_field_callback() {
		?>
        <input type="text"
               class="regular-text"
               name="<?php echo esc_attr( Kindful_Login::PREFIX . Kindful_Login::ORGANIZATION_SUBDOMAIN_FIELD_NAME ); ?>"
               value="<?php esc_html_e( Kindful_Login::get_organization_subdomain() ); ?>">
		<?php
	}

	public function application_id_field_callback() {
		?>
		<input type="text"
		       class="regular-text"
		       name="<?php echo esc_attr( Kindful_Login::PREFIX . Kindful_Login::APPLICATION_ID_FIELD_NAME ); ?>"
		       value="<?php esc_html_e( Kindful_Login::get_application_id() ); ?>">
		<?php
	}

	public function secret_field_callback() {
		?>
		<input type="text"
		       class="regular-text"
		       name="<?php echo esc_attr( Kindful_Login::PREFIX . Kindful_Login::SECRET_FIELD_NAME ); ?>"
		       value="<?php esc_html_e( Kindful_Login::get_secret() ); ?>">
		<?php
	}

	public function callback_url_field_callback() {

	    $value = Kindful_Login::get_callback_url();

	    if ( empty( $value ) ) {

	        $value = home_url( '/' );
	    }
		?>
		<input type="text"
		       class="regular-text"
		       name="<?php echo esc_attr( Kindful_Login::PREFIX . Kindful_Login::CALLBACK_URL_FIELD_NAME ); ?>"
		       value="<?php esc_html_e( $value ); ?>">
		<?php
	}

	public function sanitize_url_field( $value ) {

		if ( filter_var( $value, FILTER_VALIDATE_URL ) === false ) {

			add_settings_error(
				Kindful_Login::PREFIX . Kindful_Login::CALLBACK_URL_FIELD_NAME,
				'settings_updated',
				__( 'Callback URL must be a valid URL.', 'kindful-login' ),
				'error'
			);

			return Kindful_Login::get_callback_url();
		}

		return $value;
	}
}
