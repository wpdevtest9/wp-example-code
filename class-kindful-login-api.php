<?php

class Kindful_Login_API {

	private function __construct() {
	}

	private function __clone() {
	}

	private function __wakeup() {
	}

	public static function get_instance() {

		static $instance = null;

		if ( null === $instance ) {

			$instance = new Kindful_Login_API();
		}

		return $instance;
	}

	public function get_token( $code ) {

		return self::curl( 'https://app.kindful.com/oauth2/token', array(
			'client_id'     => Kindful_Login::get_application_id(),
			'client_secret' => Kindful_Login::get_secret(),
			'code'          => $code,
			'grant_type'    => 'authorization_code',
			'redirect_uri'  => Kindful_Login::get_callback_url()
		) );
	}

	public function get_details( $token ) {

		$organization_subdomain = Kindful_Login::get_organization_subdomain();

		if ( empty( $organization_subdomain ) ) {

			return;
		}

		return self::curl( 'https://' . Kindful_Login::get_organization_subdomain() . '.kindful.com/oauth2/api/v1/details', array(), array(
			'Authorization: Bearer ' . $token
		) );
	}

	public function get_authorize_url() {

		$organization_subdomain = Kindful_Login::get_organization_subdomain();
		$application_id         = Kindful_Login::get_application_id();
		$callback_url           = Kindful_Login::get_callback_url();

		if ( empty( $organization_subdomain ) || empty( $application_id ) || empty( $callback_url ) ) {

			return;
		}

		$callback_url = urlencode( $callback_url );

		return 'https://' . $organization_subdomain . '.kindful.com/oauth2/authorize?response_type=code&client_id=' . $application_id . '&redirect_uri=' . $callback_url;
	}

	private function curl( $url, $post_data = array(), $headers = array() ) {

		$ch = curl_init( $url );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		if ( ! empty( $post_data ) ) {

			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $post_data ) );
		}

		if ( ! empty( $headers ) ) {

			$headers[] = 'Content-Type: application/json';

			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		}

		$response = curl_exec( $ch );

		return json_decode( $response );
	}
}