<?php

class Kindful_Login_Public {

	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_shortcode( 'kindful_login_uri', array( $this, 'add_shortcode' ) );
	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kindful-login-public.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kindful-login-public.js', array( 'jquery' ), $this->version, false );
	}

	public function add_shortcode( $atts ) {

		return Kindful_Login_API::get_instance()->get_authorize_url();
	}

	public function code_listener() {

		if ( empty( $_GET['code'] ) || is_user_logged_in() ) {

			return;
		}

		$token = Kindful_Login_API::get_instance()->get_token( $_GET['code'] );

		if ( empty( $token->access_token ) ) {

			return;
		}

		$details = Kindful_Login_API::get_instance()->get_details( $token->access_token );

		if ( empty( $details->email ) ) {

			return;
		}

		$user = get_user_by( 'email', $details->email );

		if ( ! empty( $user->ID ) ) {

			$this->login_user( $user->ID );
		} else {

			$user_id = $this->register_user( $details );

			if ( is_wp_error( $user_id ) ) {

				return;
			}

			if ( ! empty( $details->id ) ) {

				update_user_meta( $user_id, 'kindful_id', $details->id );
			}

			$this->login_user( $user_id );
		}
	}

	private function register_user( $details ) {

		$user_data = array(
			'user_login'   => wp_slash( $details->email ),
			'user_email'   => wp_slash( $details->email ),
			'user_pass'    => wp_generate_password( 12, false )
		);

		if ( ! empty( $details->name ) ) {

			$user_data['display_name'] = $details->name;
		}

		return wp_insert_user( $user_data );
	}

	private function login_user( $user_id ) {

		wp_clear_auth_cookie();
		wp_set_current_user( $user_id );
		wp_set_auth_cookie( $user_id );
	}
}
